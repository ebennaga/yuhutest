import * as firebase from 'firebase';
const config = {
  apiKey: "AIzaSyBpDI1K5hDX3VBnEa19J7C5qZS6gVS7qgk",
    authDomain: "lelang-a792e.firebaseapp.com",
    databaseURL: "https://lelang-a792e.firebaseio.com",
    projectId: "lelang-a792e",
    storageBucket: "lelang-a792e.appspot.com",
    messagingSenderId: "719001392892"

};
firebase.initializeApp(config);

export const database = firebase.database().ref('posts/');
export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();
export const twitterProvider = new firebase.auth.TwitterAuthProvider();
